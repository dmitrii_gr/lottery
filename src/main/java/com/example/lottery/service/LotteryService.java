package com.example.lottery.service;

import com.example.lottery.domain.Participant;
import com.example.lottery.dto.ParticipantDTO;
import com.example.lottery.dto.WinnerDTO;
import com.example.lottery.exception.LotteryException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;
import java.util.Random;

@Service
@RequiredArgsConstructor
@Slf4j
public class LotteryService {

    public static final int MAX_SUM = 999;
    private final ModelMapper modelMapper;
    private final ParticipantService participantService;
    private final RestTemplate restTemplate;

    public WinnerDTO start() {
        List<Participant> allParticipants = participantService.getAllParticipants();

        if (allParticipants.size() < 2) {
            throw new LotteryException("Количество участников меньше допустимого значения", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        Integer generateSum = getRandom(MAX_SUM);
        Integer generateParticipant = getRandom(allParticipants.size());
        log.info("generateParticipant: {} ; generateSum: {}", generateParticipant, generateSum);

        Participant participant = participantService.getParticipantById(generateParticipant);
        return new WinnerDTO(generateSum, modelMapper.map(participant, ParticipantDTO.class));

    }

    private Integer getRandom(int maxNumber) {
        String urlGenerate = "https://www.random.org/integers/?num=1&min=1&max=" + maxNumber + "&col=1&base=10&format=plain&rnd=new";
        log.info(urlGenerate);
        try {
            return Integer.valueOf(Objects.requireNonNull(restTemplate.getForObject(urlGenerate, String.class)).trim());
        } catch (RestClientException | NumberFormatException e) {
            log.info("Неудачная попытка получить число со стороннего ресурса: {}", e);
        }
        return new Random().nextInt(MAX_SUM);
    }
}
