package com.example.lottery.service;

import com.example.lottery.domain.Participant;
import com.example.lottery.dto.ParticipantDTO;
import com.example.lottery.exception.LotteryException;
import com.example.lottery.repository.ParticipantRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ParticipantService {

    private final ParticipantRepository participantRepository;

    /**
     * Получить всех участников лотереи
     *
     */
    public Participant getParticipantById(long id) {
        return participantRepository.findById(id)
                .orElseThrow(() -> new LotteryException("Пользователь с id="+id+" не существует"));
    }
    /**
     * Получить всех участников лотереи
     *
     */
    public List<Participant> getAllParticipants() {
        return participantRepository.findAll();
    }

    /**
     * Добавить нового участника
     *
     */
    public Participant addParticipant(ParticipantDTO participantDTO) {

        Participant newUser = Participant.builder()
                .name(participantDTO.getName())
                .age(participantDTO.getAge())
                .city(participantDTO.getCity())
                .build();

        return participantRepository.save(newUser);

    }


}
