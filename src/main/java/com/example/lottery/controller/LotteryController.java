package com.example.lottery.controller;

import com.example.lottery.domain.Participant;
import com.example.lottery.dto.ParticipantDTO;
import com.example.lottery.dto.WinnerDTO;
import com.example.lottery.service.LotteryService;
import com.example.lottery.service.ParticipantService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/lottery")
@RequiredArgsConstructor
public class LotteryController {
    private final ModelMapper modelMapper;
    private final ParticipantService participantService;
    private final LotteryService lotteryService;

    @GetMapping("/participant")
    public List<ParticipantDTO> getAll() {
        return participantService.getAllParticipants().stream()
                .map(participant -> modelMapper.map(participant, ParticipantDTO.class))
                .collect(Collectors.toList());
    }

    @GetMapping("/start")
    public WinnerDTO start() {
        return  lotteryService.start();
    }

    @PostMapping("/participant")
    public ResponseEntity<ParticipantDTO> newParticipant(@RequestBody ParticipantDTO participantDTO) {

        Participant participant = participantService.addParticipant(participantDTO);

        return ResponseEntity.status(HttpStatus.OK)
                .body(modelMapper.map(participant, ParticipantDTO.class));
    }

}
