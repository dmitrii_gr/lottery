package com.example.lottery.exceptioncontroller;

import com.example.lottery.exception.LotteryException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleException(Exception ex) {
        log.error("handleException: {}", ex);
        return new ResponseEntity<>("Something went wrong", new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(LotteryException.class)
    protected ResponseEntity<Object> handleResourceNotFoundException(LotteryException ex) {
        log.error("handleLotteryException: {}", ex);
        return new ResponseEntity<>("BusinessLogic Error", new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
