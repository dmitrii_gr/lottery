package com.example.lottery.exception;

import org.springframework.http.HttpStatus;

public class LotteryException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private final String message;
  private final HttpStatus httpStatus;

  public LotteryException(String message, HttpStatus httpStatus) {
    this.message = message;
    this.httpStatus = httpStatus;
  }
  public LotteryException(String message) {
    this.message = message;
    this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
  }

  @Override
  public String getMessage() {
    return message;
  }

  public HttpStatus getHttpStatus() {
    return httpStatus;
  }

}
